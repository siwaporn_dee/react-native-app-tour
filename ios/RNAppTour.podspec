require 'json'

package = JSON.parse(File.read(File.join(__dir__, '../package.json')))

Pod::Spec.new do |s|
  s.name                   = 'RNAppTour'
  s.version                = '1.0.1'
  s.summary                = 'summary'
  s.description            = 'description'
  s.homepage               = 'https://gitlab.com/siwaporn_dee/react-native-app-tour.git'
  s.license                = package['license']
  s.author                 = package['author']
  s.source                 = { :git => 'https://gitlab.com/siwaporn_dee/react-native-app-tour.git', :tag => s.version }

  s.platform               = :ios, '9.0'
  s.ios.deployment_target  = '8.0'

  s.preserve_paths         = 'LICENSE', 'package.json'
  s.source_files           = '**/*.{h,m}', '**/MaterialShowcase/*.swift','**/*.framework'
  s.requires_arc    = true

  s.dependency             'React'

  #s.vendored_frameworks = 'MaterialShowcase.framework'
  #s.frameworks = 'MaterialShowcase.framework'
  #s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '/Applications/Xcode.app/Contents/Developer/Library/Frameworks' }

end